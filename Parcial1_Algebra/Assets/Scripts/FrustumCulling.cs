﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity;
using CustomMath;

public class FrustumCulling : MonoBehaviour
{
    public GameObject[] goList;

    enum positions
    {
        near,
        far,
        left,
        right,
        down,
        up
    }

    enum pointsPos
    {
        N_LeftDown,
        N_LeftUp,
        N_RightUp,
        N_RightDown,
        F_LeftDown,
        F_LeftUp,
        F_RightUp,
        F_RightDown,
    }

    int amountPlanes = 6;
    int amountPoints = 8;

    int frustumLayer = 8;

    public GameObject[] planesGO;
    public Transform[] pointsGO;
    MyPlane[] planes;
    Vec3[] points;

    public Transform[] t;

    public float maxDistance;
    public float minDistance;
    public float fieldOfView;
    public float closeDiffInXFromCenter;
    public float closeDiffInYFromCenter;

    public bool IsVisible(GameObject go)
    {
        return ObjectInRoom.CheckOneVertexInRoom(go, planes, planes.Length);
    }

    public void CreatePoints(Transform tf)
    {
        Vec3 pos = new Vec3(tf.position.x, tf.position.y, tf.position.z);

        points[(int)pointsPos.N_LeftDown] = pos + new Vec3(-closeDiffInXFromCenter, -closeDiffInYFromCenter, minDistance);
        points[(int)pointsPos.N_LeftUp] = pos + new Vec3(-closeDiffInXFromCenter, closeDiffInYFromCenter, minDistance);
        points[(int)pointsPos.N_RightUp] = pos + new Vec3(closeDiffInXFromCenter, closeDiffInYFromCenter, minDistance);
        points[(int)pointsPos.N_RightDown] = pos + new Vec3(closeDiffInXFromCenter, -closeDiffInYFromCenter, minDistance);
        points[(int)pointsPos.F_LeftDown] = pos + new Vec3(-fieldOfView / 2f, -fieldOfView / 2f, maxDistance);
        points[(int)pointsPos.F_LeftUp] = pos + new Vec3(-fieldOfView / 2f, fieldOfView / 2f, maxDistance);
        points[(int)pointsPos.F_RightUp] = pos + new Vec3(fieldOfView / 2f, fieldOfView / 2f, maxDistance);
        points[(int)pointsPos.F_RightDown] = pos + new Vec3(fieldOfView / 2f, -fieldOfView / 2f, maxDistance);
    }

    public void CreatePlanes()
    {
        MyPlane pToGetPlaneNormal = new MyPlane(points[(int)pointsPos.N_RightDown], points[(int)pointsPos.N_RightUp], points[(int)pointsPos.N_LeftUp]);
        planes[(int)positions.near] = new MyPlane(pToGetPlaneNormal.normal, points[(int)pointsPos.N_LeftUp]);

        pToGetPlaneNormal = new MyPlane(points[(int)pointsPos.F_LeftDown], points[(int)pointsPos.F_LeftUp], points[(int)pointsPos.F_RightUp]);
        planes[(int)positions.far] = new MyPlane(pToGetPlaneNormal.normal, points[(int)pointsPos.F_RightDown]);

        pToGetPlaneNormal = new MyPlane(points[(int)pointsPos.N_LeftUp], points[(int)pointsPos.F_LeftUp], points[(int)pointsPos.F_LeftDown]);
        planes[(int)positions.left] = new MyPlane(pToGetPlaneNormal.normal, points[(int)pointsPos.N_LeftUp]);

        pToGetPlaneNormal = new MyPlane(points[(int)pointsPos.F_RightDown], points[(int)pointsPos.F_RightUp], points[(int)pointsPos.N_RightUp]);
        planes[(int)positions.right] = new MyPlane(pToGetPlaneNormal.normal, points[(int)pointsPos.F_RightDown]);

        pToGetPlaneNormal = new MyPlane(points[(int)pointsPos.N_LeftDown], points[(int)pointsPos.F_LeftDown], points[(int)pointsPos.F_RightDown]);
        planes[(int)positions.down] = new MyPlane(pToGetPlaneNormal.normal, points[(int)pointsPos.F_RightDown]);

        pToGetPlaneNormal = new MyPlane(points[(int)pointsPos.F_RightUp], points[(int)pointsPos.F_LeftUp], points[(int)pointsPos.N_LeftUp]);
        planes[(int)positions.up] = new MyPlane(pToGetPlaneNormal.normal, points[(int)pointsPos.N_LeftUp]);

    }

    void CreateGOPoints()
    {
        pointsGO[(int)pointsPos.N_LeftDown].transform.position = points[(int)pointsPos.N_LeftDown];
        pointsGO[(int)pointsPos.N_LeftUp].transform.position = points[(int)pointsPos.N_LeftUp];
        pointsGO[(int)pointsPos.N_RightUp].transform.position = points[(int)pointsPos.N_RightUp];
        pointsGO[(int)pointsPos.N_RightDown].transform.position = points[(int)pointsPos.N_RightDown];
        pointsGO[(int)pointsPos.F_LeftDown].transform.position = points[(int)pointsPos.F_LeftDown];
        pointsGO[(int)pointsPos.F_LeftUp].transform.position = points[(int)pointsPos.F_LeftUp];
        pointsGO[(int)pointsPos.F_RightUp].transform.position = points[(int)pointsPos.F_RightUp];
        pointsGO[(int)pointsPos.F_RightDown].transform.position = points[(int)pointsPos.F_RightDown];
    }

    void CreateGOPlanes()
    {
        planesGO[(int)positions.near].transform.localPosition = new Vec3(0, 0, minDistance);
        planesGO[(int)positions.far].transform.localPosition = new Vec3(0, 0, maxDistance);
        planesGO[(int)positions.left].transform.eulerAngles = new Vec3(0, -fieldOfView, -90);
        planesGO[(int)positions.right].transform.eulerAngles = new Vec3(0, fieldOfView, 90);
        planesGO[(int)positions.down].transform.eulerAngles = new Vec3(fieldOfView, 0, 0);
        planesGO[(int)positions.up].transform.eulerAngles = new Vec3(180 - fieldOfView, 0, 0);
    }

    void ConvertPointsAndPlanes()
    {
        int i = (int)pointsPos.N_LeftDown;
        Vec3 pos = new Vec3(pointsGO[i].position.x, pointsGO[i].position.y, pointsGO[i].position.z);
        points[i] = pos;

        i = (int)pointsPos.N_LeftUp;
        pos = new Vec3(pointsGO[i].position.x, pointsGO[i].position.y, pointsGO[i].position.z);
        points[i] = pos;

        i = (int)pointsPos.N_RightUp;
        pos = new Vec3(pointsGO[i].position.x, pointsGO[i].position.y, pointsGO[i].position.z);
        points[i] = pos;

        i = (int)pointsPos.N_RightDown;
        pos = new Vec3(pointsGO[i].position.x, pointsGO[i].position.y, pointsGO[i].position.z);
        points[i] = pos;

        i = (int)pointsPos.F_LeftDown;
        pos = new Vec3(pointsGO[i].position.x, pointsGO[i].position.y, pointsGO[i].position.z);
        points[i] = pos;
        i = (int)pointsPos.F_LeftUp;
        pos = new Vec3(pointsGO[i].position.x, pointsGO[i].position.y, pointsGO[i].position.z);
        points[i] = pos;
        i = (int)pointsPos.F_RightUp;
        pos = new Vec3(pointsGO[i].position.x, pointsGO[i].position.y, pointsGO[i].position.z);
        points[i] = pos;
        i = (int)pointsPos.F_RightDown;
        pos = new Vec3(pointsGO[i].position.x, pointsGO[i].position.y, pointsGO[i].position.z);
        points[i] = pos;

        CreatePlanes();
    }

    void DrawFrustum()
    {
        Debug.DrawLine(points[(int)pointsPos.N_LeftUp], points[(int)pointsPos.F_LeftUp], Color.red);
        Debug.DrawLine(points[(int)pointsPos.N_LeftDown], points[(int)pointsPos.F_LeftDown], Color.red);
        Debug.DrawLine(points[(int)pointsPos.F_LeftDown], points[(int)pointsPos.F_LeftUp], Color.red);
        Debug.DrawLine(points[(int)pointsPos.N_RightUp], points[(int)pointsPos.F_RightUp], Color.red);
        Debug.DrawLine(points[(int)pointsPos.F_LeftUp], points[(int)pointsPos.F_RightUp], Color.red);
        Debug.DrawLine(points[(int)pointsPos.N_RightDown], points[(int)pointsPos.F_RightDown], Color.red);
        Debug.DrawLine(points[(int)pointsPos.F_RightUp], points[(int)pointsPos.F_RightDown], Color.red);
        Debug.DrawLine(points[(int)pointsPos.F_LeftDown], points[(int)pointsPos.F_RightDown], Color.red);
        Debug.DrawLine(points[(int)pointsPos.N_LeftDown], points[(int)pointsPos.N_LeftUp], Color.red);
        Debug.DrawLine(points[(int)pointsPos.N_LeftUp], points[(int)pointsPos.N_RightUp], Color.red);
        Debug.DrawLine(points[(int)pointsPos.N_RightUp], points[(int)pointsPos.N_RightDown], Color.red);
        Debug.DrawLine(points[(int)pointsPos.N_RightDown], points[(int)pointsPos.N_LeftDown], Color.red);
    }

    void DrawNormalsOfPlanes()
    {
        //normal 1
        Debug.DrawLine(transform.position, transform.position + planes[(int)positions.near].normal, Color.green);
        Debug.DrawLine(transform.position, transform.position + planes[(int)positions.near].normal / 2f, Color.blue);

        //normal 2
        Debug.DrawLine(transform.position + new Vec3(-fieldOfView / 2f, 0f, maxDistance / 2f), transform.position + new Vec3(-fieldOfView / 2f, 0f, maxDistance / 2f) + planes[(int)positions.left].normal, Color.green);
        Debug.DrawLine(transform.position + new Vec3(-fieldOfView / 2f, 0f, maxDistance / 2f), transform.position + new Vec3(-fieldOfView / 2f, 0f, maxDistance / 2f) + planes[(int)positions.left].normal / 2f, Color.blue);

        //normal 3
        Debug.DrawLine(transform.position + new Vec3(0f, fieldOfView / 2f, maxDistance / 2f), transform.position + new Vec3(0f, fieldOfView / 2f, maxDistance / 2f) + planes[(int)positions.up].normal, Color.green);
        Debug.DrawLine(transform.position + new Vec3(0f, fieldOfView / 2f, maxDistance / 2f), transform.position + new Vec3(0f, fieldOfView / 2f, maxDistance / 2f) + planes[(int)positions.up].normal / 2f, Color.blue);

        //normal 4
        Debug.DrawLine(transform.position + new Vec3(fieldOfView / 2f, 0f, maxDistance / 2f), transform.position + new Vec3(fieldOfView / 2f, 0f, maxDistance / 2f) + planes[(int)positions.right].normal, Color.green);
        Debug.DrawLine(transform.position + new Vec3(fieldOfView / 2f, 0f, maxDistance / 2f), transform.position + new Vec3(fieldOfView / 2f, 0f, maxDistance / 2f) + planes[(int)positions.right].normal / 2f, Color.blue);

        //normal 5
        Debug.DrawLine(transform.position + new Vec3(0f, -fieldOfView / 2f, maxDistance / 2f), transform.position + new Vec3(0f, -fieldOfView / 2f, maxDistance / 2f) + planes[(int)positions.down].normal, Color.green);
        Debug.DrawLine(transform.position + new Vec3(0f, -fieldOfView / 2f, maxDistance / 2f), transform.position + new Vec3(0f, -fieldOfView / 2f, maxDistance / 2f) + planes[(int)positions.down].normal / 2f, Color.blue);

        //normal 6
        Debug.DrawLine(transform.position + new Vec3(0f, 0f, maxDistance), transform.position + new Vec3(0f, 0f, maxDistance) + planes[(int)positions.far].normal, Color.green);
        Debug.DrawLine(transform.position + new Vec3(0f, 0f, maxDistance), transform.position + new Vec3(0f, 0f, maxDistance) + planes[(int)positions.far].normal / 2f, Color.blue);
    }

    private void Start()
    {
        planes = new MyPlane[amountPlanes];
        points = new Vec3[amountPoints];

        CreatePoints(transform);
        CreateGOPoints();
        CreateGOPlanes();
        CreatePlanes();

        goList = FindObjectsOfType<GameObject>();
    } 

    private void Update()
    {
        ConvertPointsAndPlanes();

        DrawFrustum();
        DrawNormalsOfPlanes();

        for (int i = 0; i < goList.Length; i++)
        {
            if (goList[i].layer == frustumLayer)
            {
                if (IsVisible(goList[i]))
                    goList[i].GetComponent<MeshRenderer>().enabled = true;
                else
                    goList[i].GetComponent<MeshRenderer>().enabled = false;
            }
        }
    }
}

