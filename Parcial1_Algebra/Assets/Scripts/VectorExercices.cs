﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EjerciciosAlgebra;
using CustomMath;

public class VectorExercices : MonoBehaviour
{
    public enum ejercicios
    {
        uno,
        dos,
        tres,
        cuatro,
        cinco,
        seis,
        siete,
        ocho,
        nueve,
        diez
    }

    public ejercicios ejs;

    public Vector3 aV;
    public Vector3 bV;
    public Vector3 cV;

    public Vec3 a;
    public Vec3 b;
    public Vec3 c;
    float t = 0.0f;

    void Update()
    {
        a = new Vec3(aV.x, aV.y, aV.z);
        b = new Vec3(bV.x, bV.y, bV.z);
        

        switch (ejs)
        {
            case ejercicios.uno:
                c = a + b;
                break;
            case ejercicios.dos:
                c = b - a;
                break;
            case ejercicios.tres:
                c.x = a.x * b.x;
                c.y = a.y * b.y;
                c.z = a.z * b.z;
                break;
            case ejercicios.cuatro:
                c = Vec3.Cross(b, a);
                break;
            case ejercicios.cinco:
                t += Time.deltaTime;
                if (t >= 1f)
                {
                    t = 0f;
                }
                c = Vec3.Lerp(a, b, t);
                break;
            case ejercicios.seis:
                c = Vec3.Max(b, a);
                break;
            case ejercicios.siete:
                c = Vec3.Project(a, b);
                break;
            case ejercicios.ocho:
                Vec3 aux = a + b;
                aux.Normalize();
                c = aux * Vec3.Distance(a, b);
                break;
            case ejercicios.nueve:
                b.Normalize();
                c = Vec3.Reflect(a, b);
                break;
            case ejercicios.diez:
                t += Time.deltaTime;
                if (t >= 10f)
                {
                    t = 0f;
                }
                c = Vec3.LerpUnclamped(b, a, t);
                break;
            default:
                break;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (ejs == ejercicios.diez)
            {
                ejs = ejercicios.uno;
            }
            else
            {
                ejs = (ejercicios)((int)ejs + 1);
            }
        }

        cV = new Vec3(c.x, c.y, c.z);

        Debug.DrawLine(Vec3.Zero, a, Color.white);
        Debug.DrawLine(Vec3.Zero, b, Color.black);
        Debug.DrawLine(Vec3.Zero, c, Color.red);        
    }
}
