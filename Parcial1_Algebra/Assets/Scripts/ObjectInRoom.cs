﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CustomMath;

public class ObjectInRoom : MonoBehaviour
{
    const short tam = 6;

    public GameObject go;
    public GameObject indicator;

    public MyPlane[] roomPlanes = new MyPlane[tam];
    Vec3[] normal = new Vec3[tam];
    Vec3[] roomVertex = new Vec3[2];

    public static bool InRoom(Vec3 position, MyPlane[] roomPlanes, int tam)
    {
        for (int i = 0; i < tam; i++)
        {
            for (int j = 0; j < tam; j++)
            {
                if (roomPlanes[i].GetSide(position) != roomPlanes[j].GetSide(position))
                    return false;
            }            
        }

        return true;
    }

    public static bool CheckAllVertexInRoom(GameObject go, MyPlane[] roomPlanes, int tam)
    {
        Mesh mesh = go.GetComponent<MeshFilter>().mesh;
        Vector3[] vertices = mesh.vertices;

        for (int i = 0; i < vertices.Length; i++)
        {
            Vec3 pos = new Vec3(go.transform.position.x, go.transform.position.y, go.transform.position.z);
            Vec3 vertexPos = new Vec3(vertices[i].x, vertices[i].y, vertices[i].z);

            if (!InRoom(pos + vertexPos, roomPlanes, tam))
                return false;
        }

        return true;
    }

    public static bool CheckOneVertexInRoom(GameObject go, MyPlane[] roomPlanes, int tam)
    {
        Mesh mesh = go.GetComponent<MeshFilter>().mesh;
        Vector3[] vertices = mesh.vertices;

        for (int i = 0; i < vertices.Length; i++)
        {
            Vec3 pos = new Vec3(go.transform.position.x, go.transform.position.y, go.transform.position.z);
            Vec3 vertexPos = new Vec3(vertices[i].x, vertices[i].y, vertices[i].z);

            if (InRoom(pos + vertexPos, roomPlanes, tam))
                return true;
        }

        return false;
    }

    void Start()
    {
        normal[0] = new Vec3(0, 1, 0);// Floor
        normal[1] = new Vec3(0, -1, 0);// Roof
        normal[2] = new Vec3(0, 0, -1); // Front
        normal[3] = new Vec3(0, 0, 1);// Back
        normal[4] = new Vec3(-1, 0, 0); // Right
        normal[5] = new Vec3(1, 0, 0);// Left

        roomVertex[0] = new Vec3(-5, 5, 5); 
        roomVertex[1] = new Vec3(5, -5, -5);

        roomPlanes[0] = new MyPlane(normal[0], roomVertex[1]);// Floor
        roomPlanes[1] = new MyPlane(normal[1], roomVertex[0]);// Roof
        roomPlanes[2] = new MyPlane(normal[2], roomVertex[0]);// Front
        roomPlanes[3] = new MyPlane(normal[3], roomVertex[1]);// Back
        roomPlanes[4] = new MyPlane(normal[4], roomVertex[1]);// Right
        roomPlanes[5] = new MyPlane(normal[5], roomVertex[0]);// Left
    }

    void Update()
    {
        if (CheckAllVertexInRoom(go, roomPlanes, tam))
        {
            indicator.GetComponent<MeshRenderer>().material.color = Color.green;
            print("The object is completely in the room");
        }
        else if (CheckOneVertexInRoom(go, roomPlanes, tam))
        {
            indicator.GetComponent<MeshRenderer>().material.color = Color.yellow;
            print("The object is partially in the room");
        }
        else
        {
            indicator.GetComponent<MeshRenderer>().material.color = Color.red;
            print("The object is not in the room");
        }
    }
}


