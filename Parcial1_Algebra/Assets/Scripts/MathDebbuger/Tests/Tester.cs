﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MathDebbuger;
using CustomMath;
public class Tester : MonoBehaviour
{
    public Text textUnity;
    public Text textVec3;
    public Text textTitle;
    public Text vectors;

    Vector3 VU1;
    Vector3 VU2;
    Vec3 V1;
    Vec3 V2;

    [Header("To test")]
    public float valueX = 0;
    public float valueY = 0;
    public float valueZ = 0;
    public float maxLength = 1;
    public Vector3 maxVector = new Vector3(1,1,6);
    public Vector3 minVector = new Vector3(2, 1, 1);
    public Vector3 scaleVec = new Vector3(2, 2, 2);
    public float time = 1.0f;

    enum demostration
    {
        constructor2Parameters,
        constructor3Parameters,
        angle,
        clampMagnitude,
        magnitude,
        cross,
        distance,
        dot,
        lerp,
        lerpUnclamped,
        max,
        min,
        sqrMagnitude,
        project,
        reflect,
        set,
        scale,
        normalize,
        last
    }

    demostration d;

    void DrawComparations1Vector()
    {
        textUnity.text = "UnityVector1 (" + VU1.x + ", " + VU1.y + ", " + VU1.z + ")";

        textVec3.text = "MyVector1 (" + V1.x + ", " + V1.y + ", " + V1.z + ")";
    }

    void DrawComparations2Vectors()
    {
        textUnity.text = "UnityVector1 (" + VU1.x + ", " + VU1.y + ", " + VU1.z + ")\n" +
                         "UnityVector2(" + VU2.x + ", " + VU2.y + ", " + VU2.z + ")\n";

        textVec3.text = "MyVector1 (" + V1.x + ", " + V1.y + ", " + V1.z + ")\n" +
                         "MyVector2(" + V2.x + ", " + V2.y + ", " + V2.z + ")\n";
    }

    void DrawFloatValue(string nameOfValue, float valueUnity, float valueVec3)
    {
        textUnity.text = "Unity: The value of " + nameOfValue + " is " + valueUnity;
        textVec3.text = "Vec3: The value of " + nameOfValue + " is " + valueVec3;
    }

    void DrawVector3Values(string nameOfValue, Vector3 VU, Vec3 V)
    {
        textUnity.text = "Unity: The value of " + nameOfValue + " is Vector3 (" + VU.x + ", " + VU.y + ", " + VU.z + ")\n";
        textVec3.text = "Vec3: The value of " + nameOfValue + " is Vec3 (" + V.x + ", " + V.y + ", " + V.z + ")";
    }

    void DrawVectorBeforeAndAfter(string nameOfAction)
    {
        textUnity.text = "UnityVector1 un" + nameOfAction + " (" + VU1.x + ", " + VU1.y + ", " + VU1.z + ")\n" +
                         "UnityVector1 " + nameOfAction + " (" + VU2.x + ", " + VU2.y + ", " + VU2.z + ")";

        textVec3.text = "MyVector1 un" + nameOfAction + " (" + V1.x + ", " + V1.y + ", " + V1.z + ")\n" +
                        "MyVector1 " + nameOfAction + " (" + V2.x + ", " + V2.y + ", " + V2.z + ")";
    }

    void SetTitle(string title)
    {
        textTitle.text = title;
    }

    void DrawVectors()
    {
        vectors.text =  "UnityVector1 (" + VU1.x + ", " + VU1.y + ", " + VU1.z + ")\n" +
                         "UnityVector2(" + VU2.x + ", " + VU2.y + ", " + VU2.z + ")\n" +
                         "MyVector1 (" + V1.x + ", " + V1.y + ", " + V1.z + ")\n" +
                         "MyVector2(" + V2.x + ", " + V2.y + ", " + V2.z + ")\n";
    }

    void Start()
    {
        VU1 = Vector3.zero;
        VU2 = Vector3.zero;

        V1 = Vec3.Zero;
        V2 = Vec3.Zero;

        d = demostration.constructor2Parameters;
    }

    void Update()
    {
        switch (d)
        {
            case demostration.constructor2Parameters:
                {
                    SetTitle("Constructor with 2 parameters");
                    VU1 = new Vector3(valueX, valueY);
                    V1 = new Vec3(valueX, valueY);
                    DrawComparations1Vector();
                }
                break;
            case demostration.constructor3Parameters:
                {
                    SetTitle("Constructor with 3 parameters");
                    VU1 = new Vector3(valueX, valueY, valueZ);
                    V1 = new Vec3(valueX, valueY, valueZ);
                    DrawComparations1Vector();
                }
                break;
            case demostration.angle:
                {
                    VU1 = new Vector3(valueX, valueY, valueZ);
                    V1 = new Vec3(valueX, valueY, valueZ);
                    VU2 = new Vector3(valueX + 1, valueY + 1, valueZ + 1);
                    V2 = new Vec3(valueX + 1, valueY + 1, valueZ + 1);
                    SetTitle("Angle");
                    DrawFloatValue("Angle", Vector3.Angle(VU1, VU2), Vec3.Angle(V1, V2));
                }
                break;
            case demostration.clampMagnitude:
                {
                    SetTitle("Clamp Magnitude");
                    VU1 = new Vector3(valueX, valueY, valueZ);
                    V1 = new Vec3(valueX, valueY, valueZ);
                    VU2 = Vector3.ClampMagnitude(VU1, maxLength);
                    V2 = Vec3.ClampMagnitude(V1, maxLength);

                    DrawVectorBeforeAndAfter("clamped");
                }
                break;
            case demostration.magnitude:
                {
                    SetTitle("Magnitude");
                    DrawFloatValue("Magnitude", Vector3.Magnitude(VU1), Vec3.Magnitude(V1));
                }
                break;
            case demostration.cross:
                {
                    SetTitle("Cross");
                    DrawVector3Values("Cross product", Vector3.Cross(VU1, VU2), Vec3.Cross(V1, V2));
                }
                break;
            case demostration.distance:
                {
                    SetTitle("Distance");
                    DrawFloatValue("Distance", Vector3.Distance(VU1, VU2), Vec3.Distance(V1, V2)); 
                }
                break;
            case demostration.dot:
                {
                    SetTitle("Dot product");
                    DrawFloatValue("Dot product", Vector3.Dot(VU1, VU2), Vec3.Dot(V1, V2)); 
                }
                break;
            case demostration.lerp:
                {
                    SetTitle("Lerp");
                    time -= Time.deltaTime;

                    if (time <= -3f)
                        time = 1f;

                    DrawVector3Values("Lerp", Vector3.Lerp(VU1, VU2, time), Vec3.Lerp(V1, V2, time));
                }
                break;
            case demostration.lerpUnclamped:
                {
                    SetTitle("Lerp unclamped");
                    time -= Time.deltaTime;

                    if (time <= -3f)
                        time = 1f;

                    DrawVector3Values("Lerp unclamped", Vector3.LerpUnclamped(VU1, VU2, time), Vec3.LerpUnclamped(V1, V2, time));
                }
                break;
            case demostration.max:
                {
                    SetTitle("Max");
                    Vec3 maxLimitsVec3 = new Vec3(maxVector.x, maxVector.y, maxVector.z);

                    VU1 = new Vector3(valueX, valueY, valueZ);
                    V1 = new Vec3(valueX, valueY, valueZ);
                    VU2 = Vector3.Max(VU1, maxVector);
                    V2 = Vec3.Max(V1, maxLimitsVec3);

                    DrawVectorBeforeAndAfter("limited by a maximum");
                }
                break;
            case demostration.min:
                {
                    SetTitle("Min");
                    Vec3 minLimitsVec3 = new Vec3(minVector.x, minVector.y, minVector.z);

                    VU1 = new Vector3(valueX, valueY, valueZ);
                    V1 = new Vec3(valueX, valueY, valueZ);
                    VU2 = Vector3.Min(VU1, minVector);
                    V2 = Vec3.Min(V1, minLimitsVec3);

                    DrawVectorBeforeAndAfter("limited by a minimum");
                }
                break;
            case demostration.sqrMagnitude:
                {
                    SetTitle("Square magnitude");
                    DrawFloatValue("Square magnitude", Vector3.SqrMagnitude(VU1), Vec3.SqrMagnitude(V1)); 
                }
                break;
            case demostration.project:
                {
                    SetTitle("Project");
                    DrawVector3Values("Project", Vector3.Project(VU1, VU2), Vec3.Project(V1, V2));
                }
                break;
            case demostration.reflect:
                {
                    SetTitle("Reflect");
                    DrawVector3Values("Reflect", Vector3.Reflect(VU1, VU2), Vec3.Reflect(V1, V2));
                }
                break;
            case demostration.set:
                {
                    SetTitle("Set");
                    VU1.Set(valueX, valueY, valueZ);
                    V1.Set(valueX, valueY, valueZ);
                    DrawComparations1Vector();
                }
                break;
            case demostration.scale:
                {
                    SetTitle("Scale");
                    Vec3 scaleVecVec3 = new Vec3(scaleVec.x, scaleVec.y, scaleVec.z);

                    VU1.Set(valueX, valueY, valueZ);
                    V1.Set(valueX, valueY, valueZ);
                    VU2 = VU1;
                    V2 = V1;

                    VU2.Scale(scaleVec);
                    V2.Scale(scaleVecVec3);

                    DrawVectorBeforeAndAfter("scaled");
                }
                break;
            case demostration.normalize:
                {
                    SetTitle("Normalize");
                    VU1.Set(valueX, valueY, valueZ);
                    V1.Set(valueX, valueY, valueZ);
                    VU2 = VU1;
                    V2 = V1;

                    VU2.Normalize();
                    V2.Normalize();

                    DrawVectorBeforeAndAfter("normalized");
                }
                break;
            default:
                break;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            d = (demostration)((int)d + 1);

            if (d == demostration.last)
                d = demostration.constructor2Parameters;
        }

        DrawVectors();
    }
}
