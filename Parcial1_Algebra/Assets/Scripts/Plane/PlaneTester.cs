﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CustomMath;

public class PlaneTester : MonoBehaviour
{
    public Text textUnity;
    public Text textMyPlane;
    public Text textTitle;
    public Text planes;

    Plane PU1;
    Plane PU2;
    MyPlane P1;
    MyPlane P2;

    [Header("To test")]
    public Vector3 p1 = Vector3.zero;
    public Vector3 p2 = Vector3.zero;
    public Vector3 p3 = Vector3.zero;
    public Vector3 valueNormal = Vector3.zero;
    public float distance = 0;
    public Vector3 randomPoint = Vector3.zero;
    public Vector3 randomPoint2 = Vector3.zero;
    public Vector3 translation = Vector3.zero;

    public Vec3 valueNormalV3 = Vec3.Zero;
    Vector3 point1Vector3 = Vector3.zero;
    public Vec3 point1 = Vec3.Zero;
    public Vec3 point2 = Vec3.Zero;
    public Vec3 point3 = Vec3.Zero;
      
    public Vec3 translationV3 = Vec3.Zero;

    enum demostration
    {
        constructorInNormalInPoint,
        constructorInNormalDistance,
        constructor3Points,
        translatePlaneTranslation,
        closestPointOnPlane,
        flip,
        getDistanceToPoint,
        getSide,
        sameSide,
        set3Points,
        setNormalAndPosition,
        translateVec3Translation,
        last
    }

    demostration d;

    void DrawComparations1Plane()
    {
        textUnity.text = "UnityPlane1: " + PU1.ToString() + "\nflipped" + PU1.flipped;
        textMyPlane.text = "MyPlane1: " + P1.ToString() + "\nflipped" + P1.flipped;
    }

    void DrawPlaneBeforeAndAfter(string nameOfAction)
    {
        textUnity.text =    "UnityPlane1 un" + nameOfAction + PU1.ToString() + "\nflipped" + PU1.flipped + ")\n" +
                            "UnityPlane1 " + nameOfAction + PU2.ToString() + "\nflipped" + PU2.flipped + ")\n";

        textMyPlane.text =  "MyPlane1 un" + nameOfAction + P1.ToString() + "\nflipped" + P1.flipped + ")\n" +
                            "MyPlane1 " + nameOfAction + P2.ToString() + "\nflipped" + P2.flipped + ")\n";
    }

    void DrawClosestPointOnPlane(Vector3 pointVector3, Vec3 pointVec3)
    {
        textUnity.text = "Being UnityPlane1: " + PU1.ToString() + "\nflipped" + PU1.flipped + "), the closest point from this planes is " + pointVector3.ToString() + "\n";

        textMyPlane.text = "Being MyPlane1: " + P1.ToString() + "\nflipped" + P1.flipped + "), the closest point from this planes is " + pointVec3.ToString() + "\n";
    }

    void DrawFloatValue(string nameOfValue, float valueUnity, float valueMyPlane)
    {
        textUnity.text = "Unity: The value of " + nameOfValue + " is " + valueUnity;
        textMyPlane.text = "MyPlane: The value of " + nameOfValue + " is " + valueMyPlane;
    }

    void DrawBoolValue(string nameOfValue, bool valueUnity, bool valueMyPlane)
    {
        textUnity.text = "Unity: The value of " + nameOfValue + " is " + valueUnity;
        textMyPlane.text = "MyPlane: The value of " + nameOfValue + " is " + valueMyPlane;
    }

    void SetTitle(string title)
    {
        textTitle.text = title;
    }

    void DrawPlanes()
    {
        planes.text =   "UnityPlane1: " + PU1.ToString() + "\nflipped" + PU1.flipped +
                        "\nUnityPlane2: " + PU2.ToString() + "\nflipped" + PU2.flipped +
                        "\nMyPlane1: " + P1.ToString() + "\nflipped" + P1.flipped +
                        "\nMyPlane2: " + P2.ToString() + "\nflipped" + P2.flipped;
    }

    void Start()
    {
        PU1 = new Plane(valueNormalV3, point1);
        PU2 = new Plane(valueNormalV3, point1);

        P1 = new MyPlane(valueNormalV3, point1);
        P2 = new MyPlane(valueNormalV3, point1);

        d = demostration.constructorInNormalInPoint;
    }

    void Update()
    {
        point1 = new Vec3(p1.x, p1.y, p1.z);
        point2 = new Vec3(p2.x, p2.y, p2.z);
        point3 = new Vec3(p3.x, p3.y, p3.z);

        valueNormalV3 = new Vec3(valueNormal.x, valueNormal.y, valueNormal.z);
        translationV3 = new Vec3(translation.x, translation.y, translation.z);

        switch (d)
        {
            case demostration.constructorInNormalInPoint:
                {
                    SetTitle("Constructor (inNormal, InPoint)");
                    PU1 = new Plane(valueNormalV3, point1);
                    P1 = new MyPlane(valueNormalV3, point1);

                    DrawComparations1Plane();
                }
                break;
            case demostration.constructorInNormalDistance:
                {
                    SetTitle("Constructor (inNormal, distance)");
                    PU1 = new Plane(valueNormalV3, distance);
                    P1 = new MyPlane(valueNormalV3, distance);

                    DrawComparations1Plane();
                }
                break;
            case demostration.constructor3Points:
                {
                    SetTitle("Constructor (point1, point2, point3)");
                    PU1 = new Plane(point1, point2, point3);
                    P1 = new MyPlane(point1, point2, point3);

                    DrawComparations1Plane();
                }
                break;
            case demostration.translatePlaneTranslation:
                {
                    SetTitle("Translation (plane, translation)");
                    PU1 = new Plane(point1, point2, point3);
                    P1 = new MyPlane(point1, point2, point3);
                    PU2 = Plane.Translate(PU1, translationV3);
                    P2 = MyPlane.Translate(P1, translationV3);

                    DrawPlaneBeforeAndAfter("translated");
                }
                break;
            case demostration.closestPointOnPlane:
                {
                    SetTitle("Closest point on plane");

                    Vec3 pointToCheckClosestPointVec3 = new Vec3(randomPoint.x, randomPoint.y, randomPoint.z);
                    Vec3 point2ToCheckClosestPointVec3 = new Vec3(randomPoint.x + 1, randomPoint.y, randomPoint.z);

                    PU1 = new Plane(point1, point2, point3);
                    P1 = new MyPlane(point1, point2, point3);

                    point1Vector3 = PU1.ClosestPointOnPlane(randomPoint);
                    point1 = P1.ClosestPointOnPlane(pointToCheckClosestPointVec3);

                    DrawClosestPointOnPlane(point1Vector3, point1);
                }
                break;
            case demostration.flip:
                {
                    SetTitle("Flip");
                    PU2 = PU1;
                    P2 = P1;

                    PU2.Flip();
                    P2.Flip();

                    DrawPlaneBeforeAndAfter("fliped");
                }
                break;
            case demostration.getDistanceToPoint:
                {
                    SetTitle("Get distance to point");

                    Vec3 randomPointVec3 = new Vec3(randomPoint.x, randomPoint.y, randomPoint.z);
                    PU1 = new Plane(point1, point2, point3);
                    P1 = new MyPlane(point1, point2, point3);
                   
                    DrawFloatValue("distance", PU1.GetDistanceToPoint(randomPoint), P1.GetDistanceToPoint(randomPointVec3));
                }
                break;
            case demostration.getSide:
                {
                    SetTitle("Get side");

                    Vec3 randomPointVec3 = new Vec3(randomPoint.x, randomPoint.y, randomPoint.z);                    

                    PU1 = new Plane(point1, point2, point3);
                    P1 = new MyPlane(point1, point2, point3);
                    DrawBoolValue("GetSide()", PU1.GetSide(randomPoint), P1.GetSide(randomPointVec3));
                }
                break;
            case demostration.sameSide:
                {
                    SetTitle("Same side");

                    Vec3 randomPointVec3 = new Vec3(randomPoint.x, randomPoint.y, randomPoint.z);
                    Vec3 randomPoint2Vec3 = new Vec3(randomPoint2.x, randomPoint2.y, randomPoint2.z);

                    PU1 = new Plane(point1, point2, point3);
                    P1 = new MyPlane(point1, point2, point3);

                    DrawBoolValue("SameSide()", PU1.SameSide(randomPoint2, randomPoint), P1.SameSide(randomPoint2Vec3, randomPointVec3));
                }
                break;
            case demostration.set3Points:
                {
                    SetTitle("Set 3 points");
                    PU1.Set3Points(point1, point2, point3);
                    P1.Set3Points(point1, point2, point3);

                    DrawComparations1Plane();
                }
                break;
            case demostration.setNormalAndPosition:
                {
                    SetTitle("Set normal and position");
                    PU1.SetNormalAndPosition(valueNormalV3, point1);
                    P1.SetNormalAndPosition(valueNormalV3, point1);

                    DrawComparations1Plane();
                }
                break;
            case demostration.translateVec3Translation:
                { 
                    SetTitle("Translation (translation)");
                    PU1 = new Plane(point1, point2, point3);
                    P1 = new MyPlane(point1, point2, point3);
                    PU2 = PU1;
                    P2 = P1;
                    PU2.Translate(translationV3);
                    P2.Translate(translationV3);

                    DrawPlaneBeforeAndAfter("translated");
                }
                break;
            default:
                break;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            d = (demostration)((int)d + 1);

            if (d == demostration.last)
                d = demostration.constructorInNormalInPoint;
        }

        DrawPlanes();
    }
}
