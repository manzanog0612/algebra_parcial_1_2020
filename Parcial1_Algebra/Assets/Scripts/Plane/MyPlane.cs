﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CustomMath;

public struct MyPlane
{
    Vec3 p_Normal;
    float p_Distance;

    public MyPlane(Vec3 inNormal, Vec3 inPoint)
    {
        inNormal.Normalize();
        p_Normal = inNormal;
        p_Distance = -Vec3.Dot(p_Normal, inPoint);
    }

    public MyPlane(Vec3 inNormal, float d)
    {
        inNormal.Normalize();
        p_Normal = inNormal;
        p_Distance = d;
    }

    // Creates a plane.
    public MyPlane(Vec3 a, Vec3 b, Vec3 c)
    {
        Vec3 crossNormalized = Vec3.Cross(b - a, c - a);
        crossNormalized.Normalize();
        p_Normal = crossNormalized;
        p_Distance = -Vec3.Dot(p_Normal, a);
    }

    public Vec3 normal
    {
        get { return p_Normal; }
        set { p_Normal = value; }
    }
    public float distance
    {
        get { return p_Distance; }
        set { p_Distance = value; }
    }

    public MyPlane flipped
    {
        get { return new MyPlane(-p_Normal, -p_Distance); }
    }

    public static MyPlane Translate(MyPlane plane, Vec3 translation)
    {
        return new MyPlane(plane.p_Normal, plane.p_Distance += Vec3.Dot(plane.p_Normal, translation));
    }

    public Vec3 ClosestPointOnPlane(Vec3 point)
    {
        float pointPlaneDistance = -Vec3.Magnitude(Vec3.Project(point, p_Normal)) + p_Distance;
        return point - (p_Normal * pointPlaneDistance);
    }

    public void Flip()
    {
        p_Normal = -p_Normal; 
        p_Distance = -p_Distance;
    }

    public float GetDistanceToPoint(Vec3 point)
    {
        return -Vec3.Magnitude(Vec3.Project(point, p_Normal)) + p_Distance;
    }

    public bool GetSide(Vec3 point)
    {
        return Vector3.Dot(p_Normal, point) + p_Distance > 0.0f;
        //return -Vec3.Magnitude(Vec3.Project(point, p_Normal)) + p_Distance > 0.0f;
    }

    public bool SameSide(Vec3 inPt0, Vec3 inPt1)
    {
        bool pt0Side = GetSide(inPt0);
        bool pt1Side = GetSide(inPt1);

        return pt0Side == pt1Side;
    }

    public void Set3Points(Vec3 a, Vec3 b, Vec3 c)
    {
        Vec3 crossNormalized = Vec3.Cross(b - a, c - a);
        crossNormalized.Normalize();
        p_Normal = crossNormalized;
        p_Distance = -Vec3.Dot(p_Normal, a);
    }

    public void SetNormalAndPosition(Vec3 inNormal, Vec3 inPoint)
    {
        p_Normal = inNormal;
        p_Distance = -Vec3.Dot(inNormal, inPoint);
    }

    public override string ToString()
    {
        return "(normal:("+ p_Normal.x + ", " + p_Normal.y + ", " + p_Normal.z + "), distance:" + p_Distance + ")";
    }

    public string ToString(string format)
    {
        return "(normal:(" + format + ", " + format + ", " + format + "), distance:" + format + ")";
    }

    public void Translate(Vec3 translation)
    {
        p_Distance += Vec3.Dot(p_Normal, translation);
    }
}
